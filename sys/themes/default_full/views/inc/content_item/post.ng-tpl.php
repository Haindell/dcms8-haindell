<div ng-if="data.type == 'post'" id="{{data.id}}" class="post"
     ng-class="{
                             highlight:data.highlight,
                             image:data.image_html,
                             icon:data.icon_html,
                             time:data.time.show,
                             actions:data.actions.items.length > 0,
                             counter:data.counter,
                             bottom_html:data.bottom_html != '',
                             content_html:data.content_html != ''
                             }"
     data-href="{{data.url}}">

    <div class="post_image" ng-if="data.image_html" ng-bind-html="html(data.image_html)"></div>
    <div class="post_head">
        <span class="post_icon" ng-bind-html="html(data.icon_html)"></span>
        <span class="post_title" ng-bind="data.title"></span>
                <span class="post_actions" ng-if="data.actions.items.length > 0">
                    <span class="post_actions_icon"></span>
                    <span>
                        <span ng-repeat="a in data.actions.items" data-href="{{a.url}}" ng-bind="a.name"></span>
                    </span>
                </span>
        <span class="post_counter" ng-bind="data.counter"></span>
        <span class="post_time" ng-if="data.time.show">{{data.time.timestamp | date:data.time.format}}</span>
    </div>
    <div class="post_content_html" ng-if="data.content_html != ''" ng-bind-html="html(data.content_html)"></div>
    <div class="content" ng-if="data.content_items.length > 0" ng-init="content = data.content_items"
         ng-include="'content_item.html'"></div>
    <div class="post_bottom_html" ng-if="data.bottom_html != ''" ng-bind-html="html(data.bottom_html)"></div>
</div>