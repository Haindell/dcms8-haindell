<?php
class notifications_controller extends controller
{
    /**
     * @param $page_meta \page_meta
     * @return \page_meta
     */
    function theme_dependence_notifications($page_meta)
    {
        $page_meta->scripts[] =($this->getCurrentPathRel() . '/files/notifications.js');
    }

}