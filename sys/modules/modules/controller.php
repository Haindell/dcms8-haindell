<?php

class modules_controller extends controller
{
    /**
     * @param $response \response
     */
    function get_modules($response)
    {
        switch (request::getUrl()->getParam('filter', 'all')) {
            case 'installed':
                $filter = 'installed';
                $modules = modules::getInstalled();
                break;
            default:
                $filter = 'all';
                $modules = modules::getAll();
                break;
        }

        $response->page->setTitle('Список модулей');
        $response->page->breadcrumbs->add(__('Админка'), '/admin/');

        $response->page->tabs->addTab(__('Все'), request::getUrl()->setParam('filter', 'all'), $filter == 'all');
        $response->page->tabs->addTab(__('Установленные'), request::getUrl()->setParam('filter', 'installed'), $filter == 'installed');

        foreach ($modules AS $module) {
            $post = $response->page->content->addPost();
            $post->title = $module->getName();
            $post->setUrl(new url('/admin/modules/module/', array('name' => $module->getName(), 'filter' => $filter)));
        }

    }


    /**
     * @param $response \response
     * @throws Exception
     * @throws Response404Exception
     */
    function get_module($response)
    {
        $module_name = request::getUrl()->getParam('name');

        if (!$module_name)
            throw new Response404Exception();

        $module = modules::getByName($module_name);

        if (!$module)
            throw new Response404Exception();

        $response->page->breadcrumbs->add(__('Админка'), '/admin/');
        $response->page->breadcrumbs->add(__('Модули'), request::getUrl()->setPath('/admin/modules/')->removeParam('name'));


        $response->page->setTitle(__('Модуль "%s"', $module->getName()));
    }


    /**
     * Обрабатывается только для /admin/
     * @param \response $response
     */
    function response_postprocess_admin($response)
    {
        $content = $response->page->content;

        $post = $content->addPost();
        //$post->setUrl('admin/modules/');
        $post->setTitle(__('Модули'));

        $child_post = $post->addPost();
        $child_post->setTitle(__('Список модулей'));
        $child_post->setUrl('/admin/modules/all/');

        $child_post = $post->addPost();
        $child_post->setTitle(__('Приоритет модулей'));
        $child_post->setUrl('/admin/modules/priority/controller/');
    }
}