<?php

class example_controller extends controller
{
    /**
     * @param $response \response
     */
    function get_index($response)
    {
        $page = $response->page;

        $page->setTitle("Сообщение " . request::getParam('post', 'не выбрано'));

        $menu = $page->getMenu();
        for ($i = 0; $i < 20; $i++) {
            $menu->addMenuItem(__("Пункт меню %s", $i), request::modifyParam('menu', $i), request::getParam('menu') == $i);
        }

        $tabs = $page->getTabs();
        for ($i = 0; $i < 4; $i++) {
            $tabs->addTab(__("Вкладка %s", $i), request::modifyParam('tab', $i), request::getParam('tab') == $i);
        }

        $breadcrumbs = $page->getBreadcrumbs();
        for ($i = 0; $i < 4; $i++) {
            $breadcrumbs->add(__("Часть пути %s", $i), '/');
        }

        $content = $page->getContent();
        for ($i = 0; $i < 10; $i++) {
            $post = $content->addPost();
            $post->setUrl(request::modifyParam('post', $i));
            $post->setTitle(__("Сообщение %s", $i));
            $post->setContentHtml('Меню:' . request::getParam('menu', 0) . ' Вкладка:' . request::getParam('tab', 0));
            $post->setBottomHtml('bottom');
            $post->setTime(999999999);
            $post->counter = $i * 40;
            $post->highlight = request::getParam('post', -1) == $i;
        }
    }
}