<?php

class user_controller extends controller
{
    protected $_form;

    /**
     * @param $response \response
     */
    function get_info($response)
    {
        $page = $response->page;
        $page->meta->title = __('Анкета пользователя');
    }

    /**
     * @param $response
     */
    function get_profile($response)
    {
        $page = $response->page;
        $page->meta->title = __('Редактировать анкету');
    }

    /**
     * @param $response
     */
    function post_profile($response)
    {

    }

    /**
     * @param $response \response
     */
    function get_settings($response)
    {
        $page = $response->page;
        $page->meta->title = __('Изменение настроек');
    }

    /**
     * @param $response \response
     */
    function post_settings($response)
    {

    }

    /**
     * @param $response \response
     * @param bool $is_auth
     */
    protected function _add_guest_tabs($response, $is_auth)
    {
        $page = $response->page;
        $page->tabs->addTab(__('Авторизация'), '/user/auth', $is_auth);
        $page->tabs->addTab(__('Регистрация'), '/user/reg', !$is_auth);
    }

    protected function _get_auth_form()
    {
        if (!$this->_form) {
            $this->_form = new page_form();
            $this->_form->action = '/user/auth';
            $this->_form->addTextField(__('Логин'), 'login', '');
            $this->_form->addPasswordField(__('Пароль'), 'password', '');
            $this->_form->addButton(__('Авторизация'));
        }
        return $this->_form;
    }

    /**
     * @param $response \response
     */
    function get_auth($response)
    {
        $page = $response->page;
        $page->setTitle(__('Авторизация'));
        $this->_add_guest_tabs($response, true);


        if (users::isAuth()){
            $post = $page->content->addPost();
            $post->setUrl('/');
            $post->setTitle(__('На главную'));
        }else{
            $page->content->items[] = $this->_get_auth_form();
        }
    }

    /**
     * @param $response \response
     */
    function post_auth($response)
    {
        //$response->page->msg(__('Попытка авторизации'));
        $form = $this->_get_auth_form();
        $form->applyRequest($this->request);

        $login = $form->getValue('login');
        $password = $form->getValue('password');

        try{
            users::auth($login, $password);
            $response->page->msg(__('Вы успешно авторизованы'));
        }catch (Exception $e){
            $response->page->err($e->getMessage());
        }

    }

    protected function _get_reg_form()
    {
        if (!$this->_form) {
            $this->_form = new page_form();
            $this->_form->action = '/user/reg';
            $this->_form->addTextField(__('Логин'), 'login', '');
            $this->_form->addPasswordField(__('Пароль'), 'password', '');
            $this->_form->addButton(__('Регистрация'));
        }
        return $this->_form;
    }

    /**
     * @param $response \response
     */
    function get_reg($response)
    {
        $page = $response->page;
        $page->meta->title = __('Регистрация');
        $this->_add_guest_tabs($response, false);

        if (users::isAuth()){
            $post = $page->content->addPost();
            $post->setUrl('/');
            $post->setTitle(__('На главную'));
        }else{
            $page->content->items[] = $this->_get_reg_form();
        }
    }

    /**
     * @param $response \response
     */
    function post_reg($response)
    {
        $form = $this->_get_reg_form();

        $login = $form->getValue('login');
        $password = $form->getValue('password');

        try{
            users::reg($login, $password);
            $response->page->msg(__('Вы успешно зарегистрированы'));
        }catch (Exception $e){
            $response->page->err($e->getMessage());
        }
    }

    /**
     * @param $response \response
     */
    function get_logout($response)
    {
        $response->page->setTitle(__('Выход с сайта'));
    }

    /**
     * @param $response \response
     */
    function post_logout($response)
    {
        users::logout();
    }

    /**
     * @param \response $response
     */
    function response_postprocess_user(response $response)
    {
        $response->page->user = users::getCurrent();
        $user_menu = $response->page->user_menu;

        //if (!$user->group) {
        $user_menu->addMenuItem(__('Авторизация'), '/user/auth');
        $user_menu->addMenuItem(__('Регистрация'), '/user/reg');
//        } else {
//            $user_menu->addMenuItem(__('Анкета'), '/user/id' . $user->id);
//            $user_menu->addMenuItem(__('Профиль'), '/user/profile');
//            $user_menu->addMenuItem(__('Настройки'), '/user/settings');
//            $user_menu->addMenuItem(__('Выход'), '/user/logout');
//        }

    }

} 