<?php

/**
 * Манипуляция с классами
 */
abstract class classes
{

    /**
     * Возвращает путь к файлу класса по его имени.
     * Метод проверяет наличие файла в папке sys/classes, а также классы, указанные в модулях
     * @param string $class_name
     * @return boolean
     */
    public static function getClassFilePath($class_name)
    {
        if (file_exists(CLASSES_PATH . '/' . $class_name . '.class.php')) {
            return CLASSES_PATH . '/' . $class_name . '.class.php';
        }

        $classes = modules::getClasses();
        if (array_key_exists($class_name, $classes)) {
            return $classes[$class_name];
        }

        return false;
    }

    /**
     * Загрузка файла класса по его имени
     * @param string $class_name
     */
    public static function autoLoadClass($class_name)
    {
        include self::getClassFilePath($class_name);
    }

}
