<?php

/**
 * Singleton. Реализация кэширования в файлах. Работает с отложенным сохранением.
 * Все изменения в файлах кэша записываются не при каждом изменении, а только один раз перед завершением работы скрипта.
 */
class cache_file
{
    protected static $_instance;
    protected $_files = array();
    protected $_files_chmod = array();
    protected $_files_modify = array();

    private function __construct()
    {

    }

    private function __clone()
    {

    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public function get($path)
    {
        if (array_key_exists($path, $this->_files))
            return $this->_files[$path];
        return $this->_files[$path] = @unserialize(@file_get_contents($path));
    }

    public function set($path, $content, $chmod)
    {
        $this->_files[$path] = $content;
        $this->_files_chmod[$path] = $chmod;
        $this->_files_modify[$path] = true;
    }

    function __destruct()
    {
        foreach ($this->_files_modify AS $path => $write) {
            if (!$write)
                continue;

            $content = & $this->_files[$path];

            // удаленный кэш все равно вернет false, поэтому в целях незахламления папки tmp лучше файл удалить
            if (!$content) {
                @unlink($path);
                continue;
            }

            filesystem::fileWrite($path, serialize($content), true, $this->_files_chmod[$path]);
        }
    }

}

/**
 * Абстрактный класс для кэширования произвольных данных.
 * Используется отложенное сохранение данных при помощи класса cache_file
 */
abstract class cache
{

    /**
     * Получение данных из кэша
     * @param string $key
     * @return boolean
     */
    static public function get($key)
    {
        $path = self::_path($key);

        // чтение данных из файла
        if (!$data = cache_file::getInstance()->get($path))
            return false;

        // проверка актуальности данных
        if ($data ['a'] < TIME) {
            cache_file::getInstance()->set($path, false, filesystem::getChmodToWrite());
            return false;
        }

        return $data ['d'];
    }

    /**
     * Запись данных в кэш
     * @param string $key
     * @param mixed $data
     * @param int|bool $ttl
     * @return boolean
     */
    static public function set($key, $data, $ttl = false)
    {
        cache_file::getInstance()->set(self::_path($key), $data ? array('a' => $ttl + TIME, 'd' => $data) : false, filesystem::getChmodToWrite());
    }

    /**
     * Получение пути к файлу по ключу
     * @param string $key
     * @return string
     */
    static protected function _path($key)
    {
        return TMP . '/private/cache.' . urlencode($key) . '.serialized';
    }

}

/**
 * Абстрактный класс. позволяет объединять ключи данных в один кэшируемый файл.
 * Полезно для множества данных, занимающих очень мало места.
 */
abstract class cache_in_file
{

    protected static function _read($cache_name, $no_cache = false)
    {
        static $cache = array();
        if ($no_cache || !isset($cache[$cache_name])) {
            $cache[$cache_name] = cache::get($cache_name);
        }
        return $cache[$cache_name];
    }

    protected static function _max_ttl($cache)
    {
        //return 1000;
        $max_ttl = 0;
        foreach ($cache as $data) {
            $max_ttl = max($max_ttl, $data['t'] - TIME);
        }
        return $max_ttl;
    }

    public static function get($cache_name, $name, $default = false)
    {

        if (!$cache = self::_read($cache_name)) {
            return $default;
        }

        if (!isset($cache[$name])) {
            // нет такой переменной
            return $default;
        }

        if ($cache[$name]['t'] < TIME) {
            // время жизни переменной вышло
            return $default;
        }

        return $cache[$name]['v'];
    }

    /**
     * Удаление устаревших данных
     * @param array $cache
     */
    protected static function _clear(&$cache)
    {
        // удаление устаревших данных
        foreach ($cache as $name => $data) {
            if ($data['t'] >= TIME)
                continue;
            unset($cache[$name]);
        }
    }

    /**
     * Удаление всех данных
     * @param $cache_name
     */
    public static function clear($cache_name)
    {
        cache::set($cache_name, false);
        self::_read($cache_name, true);
    }

    public static function set($cache_name, $name, $val, $ttl = 0)
    {
        $cache = (array)self::_read($cache_name, true);
        self::_clear($cache);
        $cache[$name] = array('t' => $ttl + TIME, 'v' => $val);
        cache::set($cache_name, $cache, self::_max_ttl($cache));
        self::_read($cache_name, true);
        return true;
    }

}
