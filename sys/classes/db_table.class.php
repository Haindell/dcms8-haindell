<?php

/**
 * Class db_table_index
 * @property \db_table_field[] $fields
 */
class db_table_index
{
    public
            $name = "",
            $unique = false,
            $primary = false,
            $fulltext = false,
            $fields = array();

    function __construct($index_config)
    {
        $this->name = $this->_parseName($index_config);
        $this->unique = $this->_parseUnique($index_config);
        $this->primary = $this->_parsePrimary($index_config);
        $this->fulltext = $this->_parseFulltext($index_config);
        $this->fields = $this->_parseFields($index_config);
    }

    function getName()
    {
        if ($this->primary)
            return 'PRIMARY';
        if (!$this->name) {
            foreach ($this->fields as $field) {
                $this->name .= $field->getName();
            }
        }
        return $this->name;
    }

    function getSqlCreateIndexDefinition($with_fields = true)
    {
        $str_arr = array();
        if ($this->primary)
            $str_arr[] = 'PRIMARY KEY';
        else if ($this->unique)
            $str_arr[] = 'UNIQUE `' . $this->name . '`';
        else if ($this->fulltext)
            $str_arr[] = 'FULLTEXT `' . $this->name . '`';
        else
            $str_arr[] = 'KEY `' . $this->name . '`';

        if ($with_fields) {
            $field_names = array();
            foreach ($this->fields AS $field) {
                $field_names[] = '`' . $field->getName() . '`';
            }

            $str_arr[] = '(' . join(',', $field_names) . ')';
        }
        return join(' ', $str_arr);
    }

    public function _parseName($index_config)
    {
        if (!array_key_exists('name', $index_config) || !$index_config['name'])
            throw new Exception(__("Имя индекса не может быть пустым"));
        return $index_config['name'];
    }

    public function _parseUnique($index_config)
    {
        if (!array_key_exists('unique', $index_config) || !$index_config['unique'])
            return false;
        return true;
    }

    public function _parsePrimary($index_config)
    {
        if (!array_key_exists('primary', $index_config) || !$index_config['primary'])
            return false;
        return true;
    }

    public function _parseFulltext($index_config)
    {
        if (!array_key_exists('fulltext', $index_config) || !$index_config['fulltext'])
            return false;
        return true;
    }

    public function _parseFields($index_config)
    {
        if (!array_key_exists('fields', $index_config) || !$index_config['fields'] || !is_array($index_config['fields']))
            throw new Exception(__("Не указаны поля индекса"));
        return $index_config['fields'];
    }
}

class db_table_field
{
    public
            $type,
            $options = "",
            $name = "",
            $unsigned = false,
            $zerofill = false,
            $null = false,
            $default = "",
            $autoincrement = false;

    function __construct($field_config)
    {
        $this->name = $this->_parseName($field_config);
        $this->type = $this->_parseType($field_config);
        $this->options = $this->_parseOptions($field_config);
        $this->zerofill = $this->_parseZerofill($field_config);
        $this->unsigned = $this->_parseUnsigned($field_config);
        $this->null = $this->_parseNull($field_config);
        $this->default = $this->_parseDefault($field_config);
        $this->autoincrement = $this->_parseAI($field_config);
    }

    function getSqlCreateDefinition()
    {
        $str_arr = array('`' . $this->name . '`');

        $str_arr[] = $this->type . ($this->options ? '(' . $this->options . ')' : '');

        if ($this->unsigned)
            $str_arr[] = 'UNSIGNED';
        if ($this->zerofill)
            $str_arr[] = 'ZEROFILL';

        $str_arr[] = ($this->null ? '' : 'NOT') . 'NULL';

        if ($this->default)
            $str_arr[] = "DEFAULT '" . $this->default . "'";

        if ($this->autoincrement)
            $str_arr[] = "AUTOINCREMENT";

        return join('', $str_arr);
    }

    public function getSqlQueryModifyTo(db_table_field $field)
    {
        if ($this->getName() !== $field->getName())
            throw new Exception(__("Имена полей не совпадают"));

        $sql_current = $this->getSqlCreateDefinition();
        $sql_to = $field->getSqlCreateDefinition();

        if ($sql_current === $sql_to)
            return null;

        return 'CHANGE `' . $this->name . '` ' . $sql_to;
    }

    public function getSqlDropDefinition()
    {
        return 'DROP `' . $this->getName() . '`';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    protected function _parseType($field_config)
    {
        if (!array_key_exists('type', $field_config))
            return 'TEXT';
        return $field_config['type'];
    }

    protected function _parseZerofill($field_config)
    {
        if (!array_key_exists('zerofill', $field_config) || !$field_config['zerofill'])
            return false;
        return true;
    }

    protected function _parseUnsigned($field_config)
    {
        if (!array_key_exists('unsigned', $field_config) || !$field_config['unsigned'])
            return false;
        return true;
    }

    protected function _parseNull($field_config)
    {
        if (!array_key_exists('null', $field_config) || !$field_config['null'])
            return false;
        return true;
    }

    protected function _parseDefault($field_config)
    {
        if (!array_key_exists('default', $field_config))
            return '';
        return $field_config['default'];
    }

    protected function _parseAI($field_config)
    {
        if (!array_key_exists('autoincrement', $field_config) || !$field_config['autoincrement'])
            return false;
        return true;
    }

    protected function _parseName($field_config)
    {
        if (!array_key_exists('name', $field_config) || !$field_config['name'])
            throw new Exception(__("Имя поля не может быть пустым"));
        return $field_config['name'];
    }

    public function _parseOptions($field_config)
    {
        if (!array_key_exists('options', $field_config))
            return '';
        return $field_config['options'];
    }
}

/**
 * Class db_table
 * @property  \db_table_field[] fields
 * @property \db_table_index[] indexes
 */
class db_table
{
    public
            $name = "",
            $fields = array(),
            $indexes = array();

    function __construct($table_config)
    {
        if (!is_array($table_config))
            throw new Exception(__('Параметры таблицы должны быть переданы в виде ассоциативного массива'));

        if (!array_key_exists('name', $table_config) || !$table_config['name'])
            throw new Exception(__('Не указано название таблицы'));

        $this->name = (string) $table_config['name'];

        if (!array_key_exists('fields', $table_config) || !is_array($table_config['fields']))
            throw new Exception(__('Не указан список полей таблицы'));

        foreach ($table_config['fields'] AS $field_config) {
            $this->fields[] = new db_table_field($field_config);
        }

        if (!array_key_exists('indexes', $table_config) || !is_array($table_config['indexes']))
            throw new Exception(__('Не указан список индексов таблицы'));

        foreach ($table_config['indexes'] AS $index_config) {
            $this->indexes[] = new db_table_index($index_config);
        }
    }

    /**
     * 
     * @param type $name
     * @return db_table_field
     */
    public function getFieldByName($name)
    {
        foreach ($this->fields as $field) {
            if ($field->getName() === $name)
                return $field;
        }
    }

    /**
     * 
     * @param type $name
     * @return db_table_index
     */
    public function getIndexByName($name)
    {
        foreach ($this->indexes as $index) {
            if ($index->getName() === $name)
                return $index;
        }
    }

    /**
     * Добавляет поля и индексы из переданной структуры
     * @param db_table $struct
     */
    function addStructure(db_table $struct)
    {
        if ($this->getName() !== $struct->getName())
            throw new Exception(__("Имена таблиц не совпадают"));

        foreach ($struct->fields as $field) {
            $curr_field = $this->getFieldByName($field->getName());
            if (!$curr_field)
                $this->fields[] = $field;
            else if ($curr_field->getSqlCreateDefinition() !== $field->getSqlCreateDefinition())
                throw new Exception(__('В таблице "%s" уже существует поле "%s" с другой структурой', $this->name, $field->getName()));
        }

        foreach ($struct->indexes as $index) {
            $curr_index = $this->getIndexByName($index->getName());
            if (!$curr_index)
                $this->indexes[] = $index;
            else if ($curr_index->getSqlCreateIndexDefinition() !== $index->getSqlCreateIndexDefinition())
                throw new Exception(__('В таблице "$s" уже существует индекс "%s" с другой структурой', $this->name, $index->getName()));
        }
        return $this;
    }

    /**
     * Возвращает SQL запрос на создание таблицы из текущей структуры
     * @return string
     */
    function getSqlQueryCreate()
    {
        $str_arr = array('CREATE TABLE `' . $this->getName() . '`');

        foreach ($this->fields as $field) {
            $str_arr[] = $field->getSqlCreateDefinition();
        }

        foreach ($this->indexes as $index) {
            $str_arr[] = $index->getSqlCreateIndexDefinition();
        }

        return implode(' ', $str_arr);
    }

    /**
     * Возвращает SQL запрос на модификацию таблицы к переданной структуре
     * @param db_table $struct
     * @return string SQL-запрос
     */
    function getSqlQueryModifyTo(db_table $struct)
    {
        $str_arr = array('ALTER TABLE `' . $this->getName() . '`');
        $str_struct_arr = array();

        if ($this->getName() !== $struct->getName())
            throw new Exception(__("Имена таблиц не совпадают"));

        // удаление индексов
        foreach ($this->indexes as $index) {
            if (!$struct->getIndexByName($index))
                $str_struct_arr[] = 'DROP ' . $index->getSqlCreateIndexDefinition(false);
        }

        // удаление полей
        foreach ($this->fields as $field) {
            if (!$struct->getFieldByName($field->getName()))
                $str_struct_arr[] = $field->getSqlDropDefinition();
        }

        // удаление измененных индексов, которые нужно будет пересоздать
        foreach ($struct->indexes as $index) {
            $curr_index = $this->getIndexByName($index->getName());
            if ($curr_index) {
                $curr_index_def = $curr_index->getSqlCreateIndexDefinition();
                $index_def = $index->getSqlCreateIndexDefinition();
                if ($curr_index_def !== $index_def) {
                    $str_struct_arr[] = 'DROP ' . $curr_index->getSqlCreateIndexDefinition(false);
                }
            }
        }

        // добавление и модификация полей
        foreach ($struct->fields as $field) {
            $curr_field = $this->getFieldByName($field->getName());
            if (!$curr_field) {
                $str_struct_arr[] = $field->getSqlCreateDefinition();
            } else {
                $sql_modify_field = $curr_field->getSqlQueryModifyTo($field);
                if ($sql_modify_field)
                    $str_struct_arr[] = $sql_modify_field;
            }
        }

        // создание  индексов
        foreach ($struct->indexes as $index) {
            $curr_index = $this->getIndexByName($index->getName());
            if (!$curr_index) {
                $str_struct_arr[] = 'ADD' . $index->getSqlCreateIndexDefinition();
            } else {
                $curr_index_def = $curr_index->getSqlCreateIndexDefinition();
                $index_def = $index->getSqlCreateIndexDefinition();
                if ($curr_index_def !== $index_def) {
                    // создание удаленных ранее измененных индексов
                    $str_struct_arr[] = 'ADD' . $index->getSqlCreateIndexDefinition();
                }
            }
        }

        if (count($str_struct_arr) == 0)
            return null;

        $str_arr[] = implode(', ', $str_struct_arr);
        return implode(' ', $str_arr);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSqlQueryDrop()
    {
        return 'DROP TABLE `' . $this->getName() . '`';
    }
}