<?php

abstract class users
{
    static protected $_current_user;

    /**
     * @param int $id
     * @return user
     */
    public static function getUserById($id)
    {
        return new user($id);
    }

    /**
     * @param string $login
     * @return user
     * @throws Exception
     * @throws ExceptionPdoNotExists
     */
    public static function getUserByLogin($login)
    {
        $res = db::me()->prepare("SELECT * FROM `users` WHERE `login` = :login LIMIT 1");
        $res->execute(array(':login' => $login));
        $data = $res->fetch();
        if (!$data)
            throw new Exception(__('Пользователь с логином "%s" не найден', $login));
        cache_in_file::set('users', (string)$data['id'], $data, 10);

        return new user($data['id']); // дополнительного запроса в базу не будет, так как данные пользователя помещены в кэш
    }

    /**
     * @return \user
     */
    public static function getCurrent()
    {
        if (!self::$_current_user) {
            if (array_key_exists('current_user_id', $_SESSION)) {
                try {
                    self::$_current_user = new user($_SESSION['current_user_id']);
                } catch (Exception $e) {
                    unset($_SESSION['current_user_id']);
                    self::$_current_user = new user();
                }
            } else if (array_key_exists('current_user_id', $_COOKIE) && array_key_exists('current_user_token', $_COOKIE)) {
                try {
                    $user = new user($_COOKIE['current_user_id']);

                    if (!$user->checkPassword(system::decrypt($_COOKIE['current_user_token'])))
                        throw new Exception(__('Ошибка авторизации по COOKIE'));

                    $_SESSION['current_user_id'] = $_COOKIE['current_user_id'];
                    self::$_current_user = $user;
                } catch (Exception $e) {

                    setcookie('current_user_id');
                    setcookie('current_user_token');

                    self::$_current_user = new user();
                }
            } else {
                self::$_current_user = new user();
            }
        }
        return self::$_current_user;
    }

    /**
     * @return bool
     */
    public static function isAuth()
    {
        return self::getCurrent()->isAuth();
    }

    /**
     * @param string $login
     * @param string $password
     * @throws Exception
     * @throws ExceptionPdoNotExists
     */
    public static function reg($login, $password)
    {
        misc::checkLogin($login);
        misc::checkPassword($password);
        try {
            self::getUserByLogin($login);
        } catch (Exception $e) {
            // если выпало исключение, значит пользователь с таким логином не зарегистрирован. Т.е. Все путем ))

            $res = db::me()->prepare('INSERT INTO `users` (`login`) VALUES (:login)');
            $res->execute(array(':login' => $login));
            $id_user = db::me()->lastInsertId();

            if (!$id_user)
                throw new Exception(__('При регистрации пользователя возникла ошибка. Пожалуйста, попробуйте позже.'));

            $user = new user($id_user);
            $user->setPassword($password);
            self::auth($login, $password);
        }

        throw new Exception(__('Пользователь с логином "%s" уже зарегистрирован', $login));
    }

    /**
     * @param string $login
     * @param string $password
     * @param bool $saveToCookie
     * @throws Exception
     */
    public static function auth($login, $password, $saveToCookie = false)
    {
        self::logout();

        $user = self::getUserByLogin($login);
        if (!$user->checkPassword($password))
            throw new Exception(__('Вы ошиблись при вводе пароля'));

        $_SESSION['current_user_id'] = $user->id;

        if ($saveToCookie){
            setcookie('current_user_id', $user->id, TIME + 60 * 60 * 24 * 365);
            setcookie('current_user_token', system::crypt($password), TIME + 60 * 60 * 24 * 365);
        }

        self::$_current_user = $user;
    }

    public static function logout()
    {
        if (self::$_current_user) {
            self::$_current_user = null;
        }
    }

} 