<?php

/**
 * Метаданные страницы
 * Class page_meta
 * @property string title
 * @property string[] keywords
 * @property string description
 * @property string[] styles
 * @property string[] scripts
 */
class page_meta extends page_render
{
    protected $_render;
    public
        $title,
        $keywords,
        $description,
        $lang,
        $styles = array(),
        $scripts = array(),
        $favicon = '/favicon.ico';
    public $time_start, $time_output;

    public function __construct()
    {
        parent::__construct();
        $this->time_start = microtime(true);
        $modules = array();
        $this->title = system::getProperty('title', __('[Заголовок не задан]'));
        $this->_render = page_render_view::getInstance();

        $dependencies = (array)$this->_render->getTheme()->getDependencies();
        if (count($dependencies))
            $modules = modules::getInstalled();
        foreach ($dependencies as $dependence) {
            foreach ($modules as $module) {
                if (in_array($dependence, $module->getThemeDependencies())) {
                    $module_controller_instance = $module->getControllerInstance();
                    $module_dependence_handler = $module->getThemeDependenceHandler($dependence);
                    $module_controller_instance->$module_dependence_handler($this);
                    break;
                }
            }
        }

        $this->favicon = $this->_render->getTheme()->getFavicon();
        $this->styles = array_merge($this->styles, $this->_render->getTheme()->getStyles());
        $this->scripts = array_merge($this->scripts, $this->_render->getTheme()->getScripts());
    }
}

/**
 * Ссылка на страницу в блоке, где указывается путь от главной
 * Class page_breadcrumb
 * @property string url
 * @property string name
 */
class page_breadcrumb extends page_render
{
    public
        $url,
        $name,
        $is_home = false,
        $is_current = false;

    function __construct($text, $url)
    {
        parent::__construct();
        $this->name = $text;
        $this->url = $url;
    }
}

/**
 * Хлебные крошки
 * Блок, указывающий путь страницы от главной сайта
 * Class page_breadcrumbs
 * @property page_breadcrumb[] items
 */
class page_breadcrumbs extends page_render
{
    public $items = array();

    public function add($text, $url)
    {
        return $this->items[] = new page_breadcrumb($text, $url);
    }
}

/**
 * Вкладка модуля (страница).
 * Class page_tab
 * @property string url
 * @property string name
 * @property bool is_active
 */
class page_tab extends page_render
{
    public
        $url,
        $name,
        $is_active;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setUrl($url)
    {
        $this->url = (string)new url($url);
    }

}

/**
 * Блок вкладок
 * Class page_tabs
 * @property page_tab[] items
 */
class page_tabs extends page_render
{
    public $items = array();

    public function addTab($name, $url, $is_active = false)
    {
        $this->items[] = $tab = new page_tab();
        $tab->setName($name);
        $tab->setUrl($url);
        $tab->is_active = $is_active;
        return $tab;
    }
}

/**
 * Действие, которое можно применить к странице
 * Class page_action
 * @property string url
 * @property string name
 */
class page_action extends page_render
{
    public
        $url,
        $name;

}

/**
 * Список действий, которые могут быть применены к странице
 * Class page_actions
 * @property page_action[] items
 */
class page_actions extends page_render
{
    public $items = array();

}

/**
 * Сообщение
 * Class page_message
 * @property string text
 * @property bool is_error
 */
class page_message extends page_render
{
    public
        $text,
        $is_error;

    /**
     * @param $text
     * @param bool $is_error
     */
    function __construct($text, $is_error = false)
    {
        parent::__construct();
        $this->text = $text;
        $this->is_error = $is_error;
    }
}

/**
 * Список сообщений
 * Class page_messages
 * @property page_message[] items
 */
class page_messages extends page_render
{
    public $items = array();

    public function err($text)
    {
        return $this->items[] = new page_message($text, true);
    }

    public function msg($text)
    {
        return $this->items[] = new page_message($text, false);
    }
}

/**
 * Пункт меню сайта. Может содержать вложенные пункты.
 * Class page_menu_item
 * @property string url
 * @property string name
 * @property string icon
 * @property page_menu_item[] menu_items
 */
class page_menu_item extends page_render
{
    public
        $url,
        $name,
        $icon,
        $is_active = false,
        $menu_items = array();

    /**
     * Установка имени
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Установка пути
     * @param string|url $url
     */
    public function setUrl($url)
    {
        $this->url = (string)new url($url);
    }

}

/**
 * Меню
 * Class page_menu
 * @property page_menu_item items
 */
class page_menu extends page_render
{
    public $items = array();

    /**
     * @param string $name
     * @param string|url $url
     * @param bool $is_active
     * @return page_menu_item
     */
    public function addMenuItem($name, $url, $is_active = false)
    {
        $this->items[] = $item = new page_menu_item();
        $item->setName($name);
        $item->setUrl($url);
        $item->is_active = $is_active;
        return $item;
    }

}

/**
 * Блок с содержимым страницы
 * Class page_content_item
 * @property page_content_item content_items
 */
class page_content_item extends page_render
{
    public $type = 'default';
    public $content_items = array();

}

/**
 * Блок с содержимым страницы в виде HTML кода
 * Class page_content_html
 */
class page_content_html extends page_content_item
{
    public $type = 'html';
    public $html;

    public function __construct($html = '')
    {
        parent::__construct();
        $this->html = $html;
    }
}

/**
 * Содержимое страницы
 * Class page_content
 * @property page_content_item[] items
 */
class page_content extends page_render
{
    public $items = array();

    /**
     * @return page_post
     */
    public function addPost()
    {
        return $this->items[] = new page_post();
    }

    /**
     * @return page_form
     */
    public function addForm()
    {
        return $this->items[] = new page_form();
    }
}

/**
 * Пункт меню пользователя
 * Class page_user_menu_item
 */
class page_user_menu_item extends page_render
{
    public
        $url,
        $name;

}

/**
 * Меню пользователя (одноуровневое)
 * Class page_user_menu
 * @property \page_user_menu_item $items
 */
class page_user_menu extends page_menu
{
    public $items = array();

    /**
     * Добавление ссылки в меню пользователя
     * @param $name
     * @param $url
     * @return page_user_menu_item
     */
    public function addMenuItem($name, $url)
    {
        $this->items[] = $menu = new page_user_menu_item();
        $menu->name = $name;
        $menu->url = $url;
        return $menu;
    }
}

/**
 * Модель страницы сайта
 * Class page
 * @property \page_meta meta Дополнительные параметры страницы
 * @property \page_breadcrumbs $breadcrumbs
 * @property \page_tabs tabs
 * @property \page_actions actions
 * @property \page_messages messages
 * @property \page_menu menu
 * @property \page_content content
 */
class page extends page_render
{
    public
        $meta,
        $breadcrumbs,
        $tabs,
        $actions,
        $messages,
        $menu,
        $content,
        $user_menu,
        $user;

    public function __construct()
    {
        parent::__construct();
        $this->meta = new page_meta();
        $this->breadcrumbs = new page_breadcrumbs();
        $this->tabs = new page_tabs();
        $this->actions = new page_actions();
        $this->messages = new page_messages();
        $this->menu = new page_menu();
        $this->content = new page_content();
        $this->user_menu = new page_user_menu();
        //$this->user = users::getCurrent(); // в модуле user, если установлен
    }

    /**
     * @param string $content
     * @return page_content_html
     */
    public function addContentHtml($content)
    {
        return $this->content[] = new page_content_html($content);
    }

    /**
     * Добавляет на страницу сообщение об ошибке
     * @param string $text
     * @return page_message
     */
    public function err($text)
    {
        return $this->messages->err($text);
    }

    /**
     * Добавляет на страницу сообщение
     * @param string $text
     * @return page_message
     */
    public function msg($text)
    {
        return $this->messages->msg($text);
    }

    public function addReturn($text, $url)
    {
        return $this->getBreadcrumbs()->add($text, $url);
    }

    /**
     * Установка заголовка страницы
     * @param string $string
     */
    public function setTitle($string)
    {
        $this->meta->title = $string;
    }

    /**
     * @return page_menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @return page_tabs
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    /**
     * @return page_breadcrumbs
     */
    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }

    /**
     * @return page_content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getJson()
    {
        $model_key = request::getModelKey();
        if ($model_key) {
            $client_model = $this->_getModel($model_key);
            $diff_model_array = $this->_getDiffModel(json::decode(json::encode($this)), $client_model);
        }

        return json::encode(array(
            'model' => isset($diff_model_array) ? $diff_model_array : $this,
            'model_key' => $this->_saveCurrentModel()
        ));
    }

    /**
     * Сохранение текущей модели страницы.
     * Необходимо, чтобы при последующем запросе мы могли отправить только измененные данные.
     * @return string
     * @throws Exception
     */
    private function _saveCurrentModel()
    {
        $model_key = misc::getRandomPhrase();

        if (!isset($_SESSION['model_hashes']))
            $_SESSION['model_hashes'] = array();

        foreach ($_SESSION['model_hashes'] as $session_model_key => $session_model_hash) {
            if ($session_model_hash['time'] < TIME - 60)
                unset($_SESSION['model_hashes'][$session_model_key]);
        }

        $_SESSION['model_hashes'][$model_key] = array('time' => TIME, 'data' => json::decode(json::encode($this)));

        return $model_key;
    }

    /**
     * Возвращает текущую модель на клиенте по ее ключу
     * @param $model_key
     * @return null|array
     */
    private function _getModel($model_key)
    {
        if (!isset($_SESSION['model_hashes']))
            return null;
        if (!isset($_SESSION['model_hashes'][$model_key]))
            return null;
        return $_SESSION['model_hashes'][$model_key]['data'];
    }

    /**
     * Возвращает разницу в моделях страницы для модификации на клиенте
     * @param array $current_model
     * @param array $client_model
     * @return array
     */
    private function _getDiffModel($current_model, $client_model)
    {
        $assoc_array = array();

        foreach ($current_model as $current_model_key => $current_model_value) {

            // скалярное
            if (is_scalar($current_model_value) || is_null($current_model_value)) {
                if (!array_key_exists($current_model_key, $client_model) || $client_model[$current_model_key] !== $current_model_value) {
                    $assoc_array[$current_model_key] = $current_model_value;
                }
                continue;
            }

            // объект или ассоциативный массив
            if (is_array($current_model_value) && misc::isAssocArray($current_model_value)) {
                if (array_key_exists($current_model_key, $client_model) && is_array($client_model[$current_model_key]))
                    $assoc_result = $this->_getDiffModel($current_model_value, $client_model[$current_model_key]);
                else
                    $assoc_result = $current_model_value;

                if ($assoc_result)
                    $assoc_array[$current_model_key] = $assoc_result;
                continue;
            }

            // секвентальный массив
            if (is_array($current_model_value) && !misc::isAssocArray($current_model_value)) {
                if (count($current_model_value) === 0) {
                    if (array_key_exists($current_model_key, $client_model) && $client_model[$current_model_key]) {
                        $assoc_array[$current_model_key] = array();
                    }
                } else if (!array_key_exists($current_model_key, $client_model) || !is_array($client_model[$current_model_key]) || count($client_model[$current_model_key]) === 0) {
                    $assoc_array[$current_model_key] = $current_model_value;
                } else {
                    $need_modify = false;
                    $modify = array(
                        'LENGTH' => count($current_model_value), // длина списка
                        'MERGE' => array(), // дополняем или заменяем содержимое у элементов списка
                        'REPLACE' => array(), // заменяем элементы списка
                        'MOVE' => array() // перемещаем элементы списка
                    );

                    $client_model_value = (array)$client_model[$current_model_key];
                    $client_model_hashes = array();
                    for ($i = 0; $i < count($client_model_value); $i++) {
                        $client_model_hashes[] = md5(json::encode($client_model_value[$i]));
                    }

                    $current_model_hashes = array();
                    for ($i = 0; $i < count($current_model_value); $i++) {
                        $current_model_hashes[] = md5(json::encode($current_model_value[$i]));
                    }


                    for ($i = 0; $i < count($current_model_hashes); $i++) {

                        if (array_key_exists($i, $client_model_hashes) && $client_model_hashes[$i] === $current_model_hashes[$i]) {
                            // совпадение по ключу и содержимому
                            continue;
                        }

                        if (in_array($current_model_hashes[$i], $client_model_hashes)) {
                            // есть на клиенте, но в другой позиции
                            $need_modify = true;
                            $modify['MOVE'][] = array(
                                'f' => array_search($current_model_hashes[$i], $client_model_hashes),
                                't' => $i
                            );
                            continue;
                        }

                        if (is_array($current_model_value[$i]) && misc::isAssocArray($current_model_value[$i])) {
                            // элемент в списке является ассоциативным массивом (объектом)
                            if (!array_key_exists($i, $client_model_hashes) || !is_array($client_model_value[$i]) || !misc::isAssocArray($client_model_value[$i])) {
                                // на клиенте этого элемента нет или он не является объектом
                                $need_modify = true;
                                $modify['REPLACE'][] = array(
                                    'i' => $i,
                                    'c' => $current_model_value[$i]);
                                continue;
                            } else {
                                $need_modify = true;
                                $modify['MERGE'][] = array(
                                    'i' => $i,
                                    'c' => $this->_getDiffModel($current_model_value[$i], $client_model_value[$i])
                                );
                            }
                        }
                    }

                    if ($need_modify)
                        $assoc_array[$current_model_key] = array('@MODIFY' => $modify);
                }

                continue;
            }
        }

        foreach ($client_model AS $client_model_key => $client_model_value) {
            if (!array_key_exists($client_model_key, $current_model))
                $assoc_array[$client_model_key] = null;
        }

        return $assoc_array;
    }
}