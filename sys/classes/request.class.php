<?php

abstract class request
{
    static protected
        $_path;

    /**
     * Метод запроса
     * @return string
     */
    static public function getMethod()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Путь страницы без GET параметров
     * @return string
     */
    static public function getPath()
    {
        if (is_null(self::$_path)) {
            self::$_path = '/' . filter_input(INPUT_GET, 'DCMS_ROUTE');
        }
        return self::$_path;
    }

    /**
     * Текущий URL
     * @return url
     */
    static public function getUrl()
    {
        $url = new url(self::getPath(), filter_input_array(INPUT_GET));
        $url->removeParam('DCMS_ROUTE');
        return $url;
    }

    /**
     * Модификация параметра втекущем запросе.
     * Вернется путь с модифицированным параметром
     * @param string $name
     * @param string $value
     * @return url
     */
    static public function modifyParam($name, $value)
    {
        return self::getUrl()->setParam($name, $value);
    }

    /**
     * Возвращает GET параметр текущего запроса
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    static public function getParam($name, $default = null)
    {
        return self::getUrl()->getParam($name, $default);
    }

    /**
     * Значение переменной POST запроса
     * @param $name
     * @return mixed
     */
    static public function getPostVar($name)
    {
        return filter_input(INPUT_POST, $name);
    }

    /**
     * Ключ клиентской модели
     * @return string|null
     */
    static public function getModelKey()
    {
        return array_key_exists('HTTP_X_DCMS_MODEL_KEY', $_SERVER) ? $_SERVER['HTTP_X_DCMS_MODEL_KEY'] : null;
    }
}