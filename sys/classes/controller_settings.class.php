<?php

/**
 * Хранение настроек модуля
 * Все изменение пишутся сразу в файл, потому работает медленнее, чем controller_cache
 * Рекомендуется использовать только для хранения параметров
 * Class controller_settings
 */
class controller_settings
{
    protected $_module_name;
    static $_settings = array();

    /**
     * @param $module \module
     */
    function __construct($module)
    {
        $this->_module_name = $module->getName();
    }

    protected function _getSettingsPath()
    {
        return SETTINGS_PATH . '/modules/' . $this->_module_name . '.json';
    }

    protected function _readSettings()
    {
        if (!array_key_exists($this->_module_name, self::$_settings[$this->_module_name])) {
            self::$_settings[$this->_module_name] = array();
            $content = @file_get_contents($this->_getSettingsPath());
            if ($content)
                self::$_settings[$this->_module_name] = json::decode($content);
        }
        return self::$_settings[$this->_module_name];
    }

    /**
     * Получение параметра
     * @param string $name Имя параметра
     * @param mixed $default Значение, если параметр не найден
     * @return bool|mixed
     */
    function get($name, $default = false)
    {
        $settings = $this->_readSettings();

        if (!array_key_exists($name, $settings))
            return $default;

        return $settings[$name];
    }

    /**
     * Запись параметра
     * @param string $name Имя
     * @param mixed $value Значение
     */
    function set($name, $value)
    {
        $settings = $this->_readSettings();
        $settings[$name] = $value;
        $settings_json = json::encode($settings);
        filesystem::fileWrite($this->_getSettingsPath(), $settings_json);
        self::$_settings[$this->_module_name] = $settings;
    }

    /**
     * Очистка параметров модуля
     */
    function clear()
    {
        unlink($this->_getSettingsPath());
    }
}