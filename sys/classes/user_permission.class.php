<?php

/**
 * Class user_permission
 */
class user_permission
{
    protected $_is_system = false;

    public
        $key = "",
        $name = "",
        $description = "",
        $dependencies = array();

    function __construct($is_system = false)
    {
        $this->_is_system = $is_system;
    }
} 