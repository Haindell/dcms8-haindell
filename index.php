<?php

version_compare(PHP_VERSION, '5.3', '>=') or die('Требуется PHP >= 5.3');

if (@function_exists('ini_set')) {
    // игнорировать повторяющиеся ошибки
    ini_set('ignore_repeated_errors', true);

    // показываем только фатальные ошибки
    ini_set('error_reporting', E_ERROR);

    // непосредственно, включаем показ ошибок
    ini_set('display_errors', true);
}

/**
 * @const H путь к корневой директории сайта
 */
define('H', __DIR__);

require_once H . '/sys/inc/start.php';

session_start();

try {
    modules::executePreprocess();
    $response = modules::executeRequest();
    modules::executePostprocess($response);
    $response->output();
} catch (ResponseException $e) {
    // нормальное сообщение об ошибке, когда контроллер бросает исключение, что не может обработать запрос.
    $response = new response();
    $response->page->setTitle(__("Ошибка"));
    $response->page->err($e->getMessage());
    $response->output();
} catch (Exception $ex) {
    try {
        // сначала пробуем нормально отобразить страницу с исключением
        $response = new response();
        $response->page->setTitle(__("Ошибка"));
        $response->page->addContentHtml('<pre>' . print_r($e, true) . '</pre>');
        $response->output();
    } catch (Exception $e) {
        // если совсем звездец...
        header("Content-Type: text/plain");
        print_r($ex);
        print_r($e);
    }
}